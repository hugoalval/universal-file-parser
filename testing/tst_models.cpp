#include <QtTest>
#include <QList>
#include <QDebug>
#include <QVariant>
#include <unordered_map>
#include "csvparser.h"
#include "jsonparser.h"
#include "xmlparser.h"
#include "fileemptyexception.h"
// add necessary includes here

class models : public QObject {
    Q_OBJECT

public:
    models();
    ~models();

private slots:
    void test_csvparser_constructor();
    void test_jsonparser_constructor();
    void test_xmlparser_constructor();
    void test_csvparser_to_string();
    void test_jsonparser_to_string();
};

models::models() {}

models::~models() {}

void models::test_csvparser_constructor() {
    // ARRANGE
    CSVParser parser;

    // ASSERT
    QVERIFY_THROWS_EXCEPTION(FileEmptyException, parser.getFileData());
}

void models::test_jsonparser_constructor() {
    // ARRANGE
    JSONParser parser;

    // ASSERT
    QVERIFY_THROWS_EXCEPTION(FileEmptyException, parser.getFileData());
}

void models::test_xmlparser_constructor() {
    // ARRANGE
    XMLParser parser;

    // ASSERT
    QVERIFY_THROWS_EXCEPTION(FileEmptyException, parser.getFileData());
}

// This test simulates the contents of a CSV file
// We are using the setFileData() function instead
// of actually reading a file.
void models::test_csvparser_to_string() {
    // ARRANGE
    CSVParser parser;
    QList<QStringList> list;
    QStringList header;
    QStringList row;

    // ACT
    header << "cid" << "first" << "last" << "email";
    list.append(header);
    row << "1001" << "Hugo" << "Alvarez" << "xxx@xxx.xxx";
    list.append(row);
    parser.setFileData(list);

    // ASSERT
    QString actual = parser.ToString();
    QString expected = "cid || first || last || email || ";
    expected += "\n-------------------------------------------------------------";
    expected += "\n1001 || Hugo || Alvarez || xxx@xxx.xxx || \n";
    QCOMPARE(actual, expected);
}

void models::test_jsonparser_to_string() {
    // ARRANGE
    JSONParser parser;
    unordered_map<QString, QVariant> data;

    // ACT
    data["isStudent"] = QVariant(true);
    data["age"] = QVariant(25);
    data["name"] = QVariant("John Doe");
    parser.setFileData(data);

    // ASSERT
    QString actual = parser.ToString();
    QString expected = "{\n\t \"name\": \"John Doe\",\n\t \"isStudent\": true,\n\t \"age\": 25\n}";
    QCOMPARE(actual, expected);
}

QTEST_APPLESS_MAIN(models)

#include "tst_models.moc"
