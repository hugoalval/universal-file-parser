QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../app

SOURCES +=  ../app/pugixml.cpp \
            ../app/csvparser.cpp \
            ../app/jsonparser.cpp \
            ../app/xmlparser.cpp \
            tst_models.cpp
