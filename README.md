# Universal File Parser

![A black and white drawing of a folder](media/ea6877880dc54a9def1b4ef4dbf303fa.png)

## Getting Started

### Requirements

This project was built in MacOS Sonoma 14.1.2 using Qt 6.6.1.

### How to run

1.  Clone this repository using:

```bash
git clone https://gitlab.com/hugoalval/universal-file-parser.git
```

1.  Open project directory and double click the universal-file-parser.pro file
2.  Set “app” run configuration with the button right above the run button (green arrow).
