#ifndef FILEERROREXCEPTION_H
#define FILEERROREXCEPTION_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include <string>
using namespace std;

/**
 * @class FileErrorException
 * @brief Exception class for signaling file opening errors.
 *
 * The FileErrorException class is designed to be thrown when an error occurs while attempting to open a file.
 * It includes a customizable message to provide additional context about the exception.
 */
class FileErrorException {
private:
    // Message associated with the exception
    string msg_;
public:
    // Constructor and Destructor
    FileErrorException(const string& msg = "Error opening the file!") : msg_(msg) {}
    ~FileErrorException() {}

    /**
     * @brief Get the message associated with the exception.
     * @return A string containing the exception message.
     */
    string getMessage() const {
        return (msg_);
    }
};


#endif // FILEERROREXCEPTION_H
