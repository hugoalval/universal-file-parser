#ifndef FILEEMPTYEXCEPTION_H
#define FILEEMPTYEXCEPTION_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include <string>
using namespace std;

/**
 * @class FileEmptyException
 * @brief Exception class for signaling an empty file condition.
 *
 * The FileEmptyException class is designed to be thrown when an attempt is made to access or operate on an empty file.
 * It includes a customizable message to provide additional context about the exception.
 */
class FileEmptyException {
private:
    // Message associated with the exception
    string msg_;
public:
    // Constructor and Destructor
    FileEmptyException(const string& msg = "File is empty!") : msg_(msg) {}
    ~FileEmptyException() {}

    /**
     * @brief Get the message associated with the exception.
     * @return A string containing the exception message.
     */
    string getMessage() const {
        return (msg_);
    }
};


#endif // FILEEMPTYEXCEPTION_H
