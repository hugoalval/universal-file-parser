#ifndef CSVPARSER_H
#define CSVPARSER_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include "baseparser.h"
#include <QTextStream>
#include <QList>
#include <QFile>
#include <QDebug>
#include <QStringList>

/**
 * @class CSVParser
 * @brief Parser implementation for Comma-Separated Values (CSV) files.
 *
 * The CSVParser class extends the BaseParser class to provide format-specific implementation for parsing CSV files.
 * It includes functions for reading CSV files, writing to CSV files, and formatting data for display.
 */
class CSVParser : public BaseParser {
private:
    // Data structure to store CSV file contents
    QList<QStringList> fileData;
public:
    // Constructor and destructor
    CSVParser() {}
    ~CSVParser() {}

    // Getter and Setter
    QList<QStringList> getFileData() const;
    void setFileData(QList<QStringList> fileData);

    // Virtual functions implementations
    void ReadFile(QString filePath);
    void WriteToFile(QString filePath);
    QString ToString();
    void PrintToTerminal();
};


#endif // CSVPARSER_H
