#ifndef XMLPARSER_H
#define XMLPARSER_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include <QString>
#include <QList>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include "baseparser.h"
#include "pugixml.hpp"
using namespace std;

/**
 * @class XMLParser
 * @brief Parser implementation for XML files using the pugixml library.
 *
 * The XMLParser class provides functionality for parsing, reading, writing, and displaying the contents of XML files.
 * It utilizes the pugixml library for efficient XML parsing.
 * https://pugixml.org
 *
 */
class XMLParser : public BaseParser{
private:
    // String to store XML file contents
    QString xmlString;

    /**
     * @brief Recursive function to traverse an XML node and build a formatted string.
     * 
     * @param node The current XML node.
     * @param depth The depth of the current node in the XML hierarchy.
     */
    void TraverseXML(const pugi::xml_node& node, int depth = 0) {
        for (auto child = node.first_child(); child; child = child.next_sibling()) {
            for (int i = 0; i < depth; ++i) {
                xmlString += "  ";
            }
            xmlString += QString::fromStdString(child.name());

            for (auto attr = child.first_attribute(); attr; attr = attr.next_attribute()) {
                xmlString += " (" + QString::fromStdString(attr.name()) + "=" + QString::fromStdString(attr.value()) + ")";
            }

            xmlString += "\n";

            TraverseXML(child, depth + 1);
        }
    }
public:
    // Constructor and destructor
    XMLParser() {}
    ~XMLParser() {}

    /**
     * @brief Get the XML file data as a formatted string.
     * @return A string representing the XML file contents.
     */
    QString getFileData();

    /**
     * @brief Read an XML file and store its contents.
     * @param filePath The path to the XML file to be read.
     */
    void ReadFile(QString filePath);

    /**
     * @brief Write the stored XML data to a file.
     * @param filePath The path to the file to be written.
     */
    void WriteToFile(QString filePath);

    /**
     * @brief Get a formatted string representation of the XML data.
     * @return A string representing the XML data in a readable format.
     */
    QString ToString();

    /**
     * @brief Print the XML data to the terminal.
     */
    void PrintToTerminal();
};

#endif // XMLPARSER_H
