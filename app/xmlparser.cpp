
/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include "xmlparser.h"
#include "fileerrorexception.h"
#include "fileemptyexception.h"

void XMLParser::ReadFile(QString filePath) {
    pugi::xml_document document;
    string path = filePath.toStdString();
    if (document.load_file(path.c_str())) {
        TraverseXML(document);
    } else {
        throw FileErrorException("Failed to load XML file!");
    }
}

QString XMLParser::getFileData() {
    if (xmlString.isEmpty())
        throw FileEmptyException("File is empty!");

    return xmlString;
}

void XMLParser::WriteToFile(QString filePath) {
    qDebug() << "WRITETOFILE XML NOT IMPLEMENTED YET";
}

QString XMLParser::ToString() {
    if (xmlString.isEmpty())
        throw FileEmptyException();

    return xmlString;
}

void XMLParser::PrintToTerminal() {
    qDebug() << xmlString;
}
