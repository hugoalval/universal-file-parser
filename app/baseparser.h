#ifndef BASEPARSER_H
#define BASEPARSER_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include <QString>

/**
 * @class BaseParser
 * @brief Abstract base class for file parsers.
 *
 * The BaseParser class defines the common interface for all file parsers in the Universal File Parser project.
 * The CSVParser JSONParser, and XMLParser classes inherit from this class to provide format-specific
 * implementations.
 */
class BaseParser {
public:
    /**
     * @brief Pure virtual function for reading a file.
     *
     * This function is overridden by derived classes to implement file reading specific to each file format.
     *
     * @param filePath The path to the file to be read.
     */
    virtual void ReadFile(QString filePath) = 0;

    /**
     * @brief Pure virtual function for writing to a file.
     *
     * This function is overridden by derived classes to implement file writing specific to each file format.
     *
     * @param filePath The path to the file to be written.
     */
    virtual void WriteToFile(QString filePath) = 0;

    /**
     * @brief Pure virtual function for printing to the terminal.
     *
     * This function is overridden by derived classes to implement printing specific to each file format.
     */
    virtual void PrintToTerminal() = 0;
};
#endif // BASEPARSER_H
