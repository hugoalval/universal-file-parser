#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void OpenFileDialog();
    void ReadCSV(QString filePath);
    void ReadJSON(QString filePath);
    void ReadXML(QString filePath);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
