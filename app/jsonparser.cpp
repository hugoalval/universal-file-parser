
/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include "jsonparser.h"
#include "fileerrorexception.h"
#include "jsonerrorexception.h"
#include "fileemptyexception.h"

// NOTE THAT THIS ONLY PARSES A JSON FILE THAT INCLUDES A SINGLE OBJECT.
void JSONParser::ReadFile(QString filePath) {
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw FileErrorException();
    }

    QByteArray jsonData = file.readAll();
    file.close();

    QJsonParseError jsonError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &jsonError);
    if (jsonError.error != QJsonParseError::NoError)
        throw JSONErrorException("Error parsing JSON:" + jsonError.errorString().toStdString());

    // Check if the top-level JSON element is an object
    if (!jsonDoc.isObject())
        throw JSONErrorException("JSON content is not an object!");

    // Convert the top-level JSON object to a QJsonObject
    QJsonObject jsonObj = jsonDoc.object();

    // Convert QJsonObject to std::unordered_map
    for (auto it = jsonObj.begin(); it != jsonObj.end(); ++it) {
        fileData[it.key()] = it.value().toVariant();
    }
}

unordered_map<QString, QVariant> JSONParser::getFileData() {
    if (fileData.empty())
        throw FileEmptyException("File is empty!");

    return fileData;
}

void JSONParser::setFileData(unordered_map<QString, QVariant> fileData) {
    this->fileData = fileData;
}

void JSONParser::WriteToFile(QString filePath) {
    qDebug() << "JSON WRITETOFILE NOT DONE YET";
}

void JSONParser::PrintToTerminal() {
    if (fileData.empty())
        throw FileEmptyException();

    for (const auto& pair : fileData) {
        qDebug() << pair.first.toStdString() << ": " << pair.second.toString().toStdString();
    }
}

QString JSONParser::ToString() {
    if (fileData.empty())
        throw FileEmptyException("File is empty!");

    QString CSVStr = "{\n";
    for (auto iterator = fileData.begin(); iterator != fileData.end(); ++iterator) {
        const QString& key = iterator->first;
        const QVariant& value = iterator->second;
        CSVStr += "\t \"" + key + "\": ";

        if (value.userType() == QMetaType::QString)
            CSVStr += "\"" + value.toString() + "\"";
        else
            CSVStr += value.toString();

        if (std::next(iterator) != fileData.end())
            CSVStr += ",";

        CSVStr += "\n";
    }

    CSVStr += "}";

    return CSVStr;
}
