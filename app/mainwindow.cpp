
/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "fileerrorexception.h"
#include "fileemptyexception.h"
#include "jsonerrorexception.h"
#include "csvparser.h"
#include "jsonparser.h"
#include "xmlparser.h"
#include <QDir>
#include <QFileDialog>
#include <QTableWidget>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);

    // Set size constraints for better layout control.
    ui->verticalLayout->setSizeConstraint(QLayout::SetMinimumSize);
    this->setLayout(ui->verticalLayout);

    // Connect the fileChooseBtn click signal to the OpenFileDialog slot.
    connect(ui->fileChooseBtn, &QPushButton::clicked, this, &MainWindow::OpenFileDialog);

    // Set up the scroll area and file contents widget. This allows my labels to
    // overflow and still being able to see the rest of the file.
    ui->scrollArea->setWidget(ui->fileContents);
    ui->fileContents->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    ui->scrollArea->show();
}

/**
 * @brief Slot to handle the fileChooseBtn click event.
 * Opens a file dialog and reads the selected file based on its extension.
 */
void MainWindow::OpenFileDialog() {
    QString filePath = QFileDialog::getOpenFileName(this, "Open File", QDir::homePath(), "All Files (*.*);;CSV Files (*.csv)");

    // Check if a file was selected and process it based on its extension.
    if (!filePath.isEmpty()) {
        ui->filePath->setText("Selected File: " + filePath);
        filePath = filePath.toLower();
        if (filePath.endsWith(".csv"))
            ReadCSV(filePath);
        else if (filePath.endsWith(".json"))
            ReadJSON(filePath);
        else if (filePath.endsWith(".xml"))
            ReadXML(filePath);
        else {
            ui->fileContents->setText("Selected file can't be processed.");
            ui->filePath->setText("");
        }
    } else {
        ui->fileContents->setText("");
        ui->filePath->setText("No file selected.");
    }
}

/**
 * @brief Reads and displays the contents of a CSV file.
 * @param filePath The path to the CSV file.
 */
void MainWindow::ReadCSV(QString filePath) {
    CSVParser parser;
    try {
        parser.ReadFile(filePath);
    } catch (FileErrorException e) {
        qDebug() << "ERROR: " << e.getMessage();
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    } catch (FileEmptyException e) {
        qDebug() << "ERROR: " << e.getMessage();
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    }
    try {
        ui->fileContents->setText(parser.ToString());
    } catch (FileEmptyException e) {
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    }

}

/**
 * @brief Reads and displays the contents of a JSON file.
 * @param filePath The path to the JSON file.
 */
void MainWindow::ReadJSON(QString filePath) {
    JSONParser parser;
    try {
        parser.ReadFile(filePath);
    } catch (JSONErrorException e) {
        qDebug() << "ERROR: " << e.getMessage();
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    } catch (FileEmptyException e) {
        qDebug() << "ERROR: " << e.getMessage();
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    }
    try {
        ui->fileContents->setText(parser.ToString());
    } catch (FileEmptyException e) {
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    }

}

/**
 * @brief Reads and displays the contents of a XML file.
 * @param filePath The path to the XML file.
 */
void MainWindow::ReadXML(QString filePath) {
    XMLParser parser;
    try {
        parser.ReadFile(filePath);
    } catch (FileErrorException e) {
        qDebug() << "ERROR: " << e.getMessage();
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    } catch (FileEmptyException e) {
        qDebug() << "ERROR: " << e.getMessage();
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    }
    try {
        ui->fileContents->setText(parser.ToString());
    } catch (FileEmptyException e) {
        ui->fileContents->setText(QString::fromStdString(e.getMessage()));
    }
}

MainWindow::~MainWindow() {
    delete ui;
}
