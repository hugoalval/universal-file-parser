#ifndef JSONERROREXCEPTION_H
#define JSONERROREXCEPTION_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include <string>
using namespace std;

/**
 * @class JSONErrorException
 * @brief Exception class for signaling errors related to JSON parsing.
 *
 * The JSONErrorException class is designed to be thrown when an error occurs during JSON parsing operations.
 * It includes a customizable message to provide additional context about the exception.
 */
class JSONErrorException {
private:
    // Message associated with the exception
    string msg_;
public:
    // Constructor and Destructor
    JSONErrorException(const string& msg) : msg_(msg) {}
    ~JSONErrorException() {}

    /**
     * @brief Get the message associated with the exception.
     * @return A string containing the exception message.
     */
    string getMessage() const {
        return (msg_);
    }
};

#endif // JSONERROREXCEPTION_H
