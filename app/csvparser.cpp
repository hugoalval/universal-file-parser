
/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include "csvparser.h"
#include "fileemptyexception.h"
#include "fileerrorexception.h"

QList<QList<QString> > CSVParser::getFileData() const {
    if (fileData.empty())
        throw FileEmptyException("File is empty!");
    return fileData;
}

void CSVParser::setFileData(QList<QStringList> fileData) {
    this->fileData = fileData;
}

/**
 * @brief This function reads the contents of a CSV file and stores its contents
 * 
 * @param filePath The path for the file of interest.
 * 
 * @throws FileErrorException Custom exception thrown if there is any error
 *         withing the file-reading process.
 */
void CSVParser::ReadFile(QString filePath) {
    QFile file(filePath);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        throw FileErrorException("File not found!");

    QTextStream in(&file);

    while (!in.atEnd()) {
        QString line = in.readLine();
        QStringList fields = line.split(',');
        fileData.append(fields);
    }

    file.close();
}

void CSVParser::WriteToFile(QString fileName) {
    qDebug() << "WRITETOFILE CSV NOT IMPLEMENTED YET";
}

/**
 * @brief This function converts the contents of the QList into a string
 * 
 * @throws FileEmptyException Custom exception thrown if the file at hand is empty
 * 
 * @returns Formatted string with the contents of a CSV file.
 */
QString CSVParser::ToString() {
    if (fileData.empty())
        throw FileEmptyException("File is empty!");

    QString CSVStr = "";
    int count = 0;
    for (QStringList &row : fileData) {
        for (QString &cell : row)
            CSVStr += cell + " || ";

        if (count == 0)
            CSVStr += "\n-------------------------------------------------------------";
        CSVStr += "\n";
        count++;
    }

    return CSVStr;
}

/**
 * @brief This function prints the contents of the file to the terminal
 * 
 * @throws FileEmptyException Custom exception thrown if the file at hand is empty
 */
void CSVParser::PrintToTerminal() {
    if (fileData.empty())
        throw FileEmptyException();

    for (QStringList &row : fileData) {
        for (QString &cell : row) {
            qDebug() << cell << " ";
        }
    }
}

