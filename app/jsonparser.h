#ifndef JSONPARSER_H
#define JSONPARSER_H

/*
 * Assignment: Final Project
 * Name: Hugo Alvarez Valdivia
 * Date: 12/11/2023
 * Email: halvarezvaldivia@dmacc.edu
 */

#include "baseparser.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QDebug>
#include <unordered_map>
using namespace std;

/**
 * @class JSONParser
 * @brief Parser implementation for JSON files.
 *
 * The JSONParser class extends the BaseParser class to provide format-specific implementation for parsing JSON files.
 * It includes functions for reading JSON files, writing to JSON files, and formatting data for display.
 */
class JSONParser : public BaseParser {
private:
    // Data structure to store JSON file contents
    unordered_map<QString, QVariant> fileData;
public:
    // Constructor and destructor
    JSONParser() {}
    ~JSONParser() {}

    // Getter and setter
    unordered_map<QString, QVariant> getFileData();
    void setFileData(unordered_map<QString, QVariant> fileData);

    // Virtual functions implmentations
    void ReadFile(QString filePath);
    void WriteToFile(QString filePath);
    void PrintToTerminal();
    QString ToString();
};

#endif // JSONPARSER_H
